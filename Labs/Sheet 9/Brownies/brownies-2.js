$(".ingr").hide();

$("#ingr").on({
    mouseover: function() {
        $(".ingr").fadeIn(1000);
    },
    mouseout: function() {
        $(".ingr").slideUp(1000);
    }
});

$("#instr").on({
    mouseover: function() {
        $(".instr").fadeIn(1000);
    },
    mouseout: function() {
        $(".instr").slideUp(1000);
    }
});
function changeName() {
    var tableData = document.getElementsByTagName("td");
    var course = document.getElementsByTagName("input")[1];
    var newName = document.getElementsByTagName("input")[2];
    var validInput = false;

    for (var i = 0; i < tableData.length; i += 3) {
        if (tableData[i].innerText == course.value.toString()) {
            tableData[i].nextElementSibling.nextElementSibling.innerText = newName.value.toString();
            validInput = true;
        }
    }

    if (validInput == false) {
        window.alert("Invalid course!");
    }
}

function displayCourses() {
    var xhr = new XMLHttpRequest();
    xhr.onload = function() {
        if (xhr.status == 200) {
            document.getElementById("courseInfo").innerHTML = this.responseText;
        }
    };
    xhr.open("GET", "data.html", true);
    xhr.send();

    var courseInput = document.createElement("input");
    courseInput.setAttribute("type", "text");
    courseInput.setAttribute("placeholder", "Enter course number");

    var newNameInput = document.createElement("input");
    newNameInput.setAttribute("type", "text");
    newNameInput.setAttribute("placeholder", "Enter new name");

    var changeBtn = document.createElement("button");
    changeBtn.innerText = "Change Teacher Name";
    changeBtn.setAttribute("type", "button");
    changeBtn.addEventListener("click", changeName, false);

    var table = document.getElementsByTagName("table")[0];
    document.getElementsByTagName("body")[0].insertBefore(courseInput, table);
    document.getElementsByTagName("body")[0].insertBefore(newNameInput, table);
    document.getElementsByTagName("body")[0].insertBefore(changeBtn, table);
}

document.getElementById("search").addEventListener("click", displayCourses, false);
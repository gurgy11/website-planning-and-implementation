$("#search").click(function() {
    $.ajax({
        type: "GET",
        url: "data.html",
        dataType: "html",
        success: function(response) {
            $("#courseInfo").html(response);
            var courseNumber = "<input type='text' id='courseNumber' placeholder='Enter course number'>";
            var newTeacherName = "<input type='text' id='newTeacherName' placeholder='Enter new course name'>";
            var changeNameBtn = $("<input type='button' id='changeNameBtn' value='Change Course Name'/>");
            $("#courseInfo").append(courseNumber).append(newTeacherName).append(changeNameBtn);
            
            $("#changeNameBtn").click(function() {
                var validInput = false;
                $("td").each(function() {
                    if ($(this).text() == $("#courseNumber").val()) {
                        $(this).next().next().text($("#newTeacherName").val());
                        validInput = true;
                    }
                });
                if (!validInput) {
                    alert("Invalid course number given!");
                }
            });
        },
        error: function() {
            alert("Error loading HTML data!");
        }
    });
});
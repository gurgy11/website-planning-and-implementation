var body = document.getElementById("top");
var mainHeader = document.getElementsByTagName("h1")[0];
var headers = document.getElementsByTagName("h2");
var nav = document.createElement("ul");

for (var i = 0; i < headers.length; i++) {
    // Creating nav list and it's elements
    var headerText = headers[i].innerText;
    headers[i].setAttribute("id", headerText);

    var itemTextNode = document.createTextNode(headerText);
    var itemLink = document.createElement("a");
    var itemElement = document.createElement("li");

    itemLink.setAttribute("href", "#" + headerText);
    itemLink.appendChild(itemTextNode);

    itemElement.appendChild(itemLink);

    nav.appendChild(itemElement);

    // Creating "Return to top" links
    var returnTopText = document.createTextNode("Return to top");
    var returnTopLink = document.createElement("a");
    var returnTopElement = document.createElement("p");

    returnTopLink.setAttribute("href", "#top");
    returnTopLink.appendChild(returnTopText);
    returnTopElement.appendChild(returnTopLink);
    body.insertBefore(returnTopElement, headers[i + 1]);
}

body.insertBefore(nav, mainHeader);
$(document).ready(function () {
    // Student Info Code
    var studentInfoBtn = $("<input type='button' class='form-control btn btn-primary' id='studentBtn' value='Load Student Info'>");
    $("#studentInfo").append(studentInfoBtn);

    $("#studentBtn").click(function () {
        $.ajax({
            type: "GET",
            url: "students.json",
            dataType: "json",
            success: function (response) {
                var count = 1;
                $.each(response, (function(key, value) {
                    $.each(value, (function(key, student) {
                        $.each(student, (function(key, student) {
                            var number = "<td>" + count + "</td>";
                            var name = "<td>" + student.name + "</td>";
                            var school = "<td>" + student.school + "</td>";
                            var average = "<td>" + student.average + "</td>";

                            var row = "<tr>" + number + name + school + average + "</tr>";

                            $("#studentTable tbody").eq(0).append(row);

                            count++;
                        }));
                    }));
                }));
            },
            error: function () {
                alert("Error loading student info into table!");
            }
        });
    });

    // Car Info Code
    var carInfoBtn = $("<input type='button' class='form-control btn btn-primary' id='carBtn' value='Load Car Info'>");
    $("#carInfo").append(carInfoBtn);

    $("#carBtn").click(function () {
        $.ajax({
            type: "GET",
            url: "cars.xml",
            dataType: "xml",
            success: function(response) {
                $(response).find("car").each(function() {
                    var id = "<td>" + $(this).find("id").text() + "</td>";
                    var year = "<td>" + $(this).find("year").text() + "</td>";
                    var make = "<td>" + $(this).find("make").text() + "</td>";
                    var model = "<td>" + $(this).find("model").text() + "</td>";
                    var color = "<td>" + $(this).find("color").text() + "</td>";
                    var mileage = "<td>" + $(this).find("mileage").text() + "</td>";
                    var price = "<td>" + $(this).find("price").text() + "</td>";

                    var row = "<tr>" + id + year + make + model + color + mileage + price + "</tr>";

                    $("#carTable tbody").eq(0).append(row);
                });
            },
            error: function() {
                alert("Error loading student info into table!");
            }
        });
    });
});